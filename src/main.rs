use std::{
    cmp,
    collections::HashSet,
    convert::TryFrom,
    io::{self, BufRead, BufReader, BufWriter, Write},
};

use anyhow::Context;
use nalgebra::{dvector, DMatrix, DVector, DVectorSlice, MatrixXx2};
use ordered_float::OrderedFloat;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    #[structopt(default_value = "1")]
    degree: usize,
}

fn main() {
    if let Err(e) = run(Opt::from_args()) {
        eprintln!("Error: {:?}", e);
        std::process::exit(1);
    }
}

fn run(opt: Opt) -> anyhow::Result<()> {
    let degree = opt.degree;

    // Get the points we're supposed to formulise.
    let stdin = io::stdin();
    let stdin_lock = stdin.lock();
    let mut reader = BufReader::new(stdin_lock);
    let xys = get_points(&mut reader)?;

    let polynomial_coeffs = least_squares(degree + 1, xys.column(0), xys.column(1));

    let stdout = io::stdout();
    let stdout_lock = stdout.lock();
    let mut writer = BufWriter::new(stdout_lock);
    print_polynomial(&mut writer, polynomial_coeffs.column(0), 4)?;

    Ok(())
}

fn get_points(input: &mut dyn BufRead) -> anyhow::Result<MatrixXx2<f64>> {
    let mut xs = Vec::new();
    let mut ys = Vec::new();

    for (i, line) in input.lines().enumerate() {
        let (x, y) = line
            .map_err(anyhow::Error::new)
            .and_then(|s| read_line(&s))
            .with_context(|| format!("Error on line {}", i + 1))?;

        xs.push(x);
        ys.push(y);
    }

    Ok(MatrixXx2::from_iterator(
        xs.len(),
        xs.into_iter().chain(ys.into_iter()),
    ))
}

fn read_line(line: &str) -> anyhow::Result<(f64, f64)> {
    let mut tokens = line.trim().split_whitespace();

    (|| {
        let x_tok = tokens.next().context("Missing x co-ordinate")?;
        let y_tok = tokens.next().context("Missing y co-ordinate")?;

        if let Some(extra_tok) = tokens.next() {
            anyhow::bail!("Unexpected token: `{}`", extra_tok);
        }

        let x = x_tok
            .parse::<f64>()
            .with_context(|| format!("Could not parse `{}` as numeric", x_tok))?;
        let y = y_tok
            .parse::<f64>()
            .with_context(|| format!("Could not parse `{}` as numeric", y_tok))?;

        Ok((x, y))
    })()
    .with_context(|| format!("Line contents: `{}`", line))
}

fn least_squares(mut n_terms: usize, xs: DVectorSlice<f64>, ys: DVectorSlice<f64>) -> DVector<f64> {
    assert_eq!(xs.len(), ys.len());

    n_terms = cmp::min(n_terms, max_terms(xs.as_slice()));

    match n_terms {
        0 => dvector![],
        1 => dvector![ys.sum() / ys.len() as f64],
        n_terms => {
            // Set up a linear equation
            // Ma = v
            // where a is the vector of coefficients of the
            // polynomial we want to create.
            let matrix = create_matrix(n_terms, xs);
            let target_vector = create_target_vector(n_terms, xs, ys);

            let inv_matrix = matrix.try_inverse().unwrap();

            inv_matrix * target_vector
        }
    }
}

/// If you have n unique x points
/// you can fit a polynomial with
/// at most n terms. (Degree n - 1).
fn max_terms(xs: &[f64]) -> usize {
    let x_set: HashSet<OrderedFloat<f64>> = xs.iter().copied().map(OrderedFloat).collect();
    x_set.len()
}

fn create_matrix(n_terms: usize, xs: DVectorSlice<f64>) -> DMatrix<f64> {
    let mut ret = DMatrix::zeros(n_terms, n_terms);

    let power_sum: Vec<f64> = (0..=(2 * (n_terms - 1)))
        .map(|i| {
            let power = i32::try_from(i).unwrap();
            xs.iter().map(|x| x.powi(power)).sum()
        })
        .collect();

    for row in 0..n_terms {
        for col in 0..n_terms {
            *ret.get_mut((row, col)).unwrap() = power_sum[row + col];
        }
    }

    ret
}

fn create_target_vector(
    n_terms: usize,
    xs: DVectorSlice<f64>,
    ys: DVectorSlice<f64>,
) -> DVector<f64> {
    let mut ret = DVector::zeros(n_terms);

    for i in 0..n_terms {
        let power = i32::try_from(i).unwrap();
        *ret.get_mut(i).unwrap() = xs
            .iter()
            .zip(ys.iter())
            .map(|(x, y)| x.powi(power) * y)
            .sum();
    }

    ret
}

fn print_polynomial(
    w: &mut dyn Write,
    coeffs: DVectorSlice<f64>,
    precision: usize,
) -> anyhow::Result<()> {
    if coeffs.is_empty() {
        write!(w, "0")?;
    } else {
        for (i, c) in coeffs.iter().enumerate() {
            if i == 0 {
                write!(w, "{:.*}", precision, c)?;
            } else {
                write!(w, " + {:.*}x", precision, c)?;
                if i > 1 {
                    write!(w, "^{}", i)?;
                }
            }
        }
    }
    writeln!(w)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use float_eq::assert_float_eq;
    use nalgebra::{dmatrix, dvector};

    #[test]
    fn empty() {
        let polynom = least_squares(1, dvector![].column(0), dvector![].column(0));
        dbg!(polynom);
    }

    #[test]
    fn two_point_line() {
        let xys = dmatrix![
            3.0, 4.0;
            6.0, 10.0;
        ];

        let polynom = least_squares(2, xys.column(0), xys.column(1));
        assert_eq!(polynom, dvector![-2.0, 2.0]);
    }

    #[test]
    fn ten_point_perfect_line() {
        let xs: Vec<f64> = (0..10).map(|i| i as f64).collect();
        let ys: Vec<f64> = xs.iter().map(|x| 30.0 - 0.25 * x).collect();

        let polynom = least_squares(2, DVector::from(xs).column(0), DVector::from(ys).column(0));

        assert_eq!(polynom, dvector![30.0, -0.25]);
    }

    #[test]
    fn degree_3_line() {
        let xs: Vec<f64> = (0..10).map(|i| i as f64).collect();
        let ys: Vec<f64> = xs.iter().map(|x| 7.0 + 3.5 * x).collect();

        let polynom = least_squares(4, DVector::from(xs).column(0), DVector::from(ys).column(0));

        assert_float_eq!(polynom[0], 7.0, abs <= 0.00001);
        assert_float_eq!(polynom[1], 3.5, abs <= 0.00001);
        for a in polynom.iter().skip(2) {
            assert_float_eq!(*a, 0.0, abs <= 0.00001);
        }
    }

    #[test]
    fn three_point_quadratic() {
        let xs = vec![-4.3, 2.24, 5.9];
        let ys: Vec<f64> = xs.iter().map(|x| -0.3 - 0.4 * x + 2.3 * x * x).collect();

        let polynom = least_squares(3, DVector::from(xs).column(0), DVector::from(ys).column(0));

        assert_eq!(polynom.len(), 3);
        assert_float_eq!(polynom[0], -0.3, abs <= 0.00001);
        assert_float_eq!(polynom[1], -0.4, abs <= 0.00001);
        assert_float_eq!(polynom[2], 2.3, abs <= 0.00001);
    }

    #[test]
    fn three_point_septic() {
        let xs = vec![-4.3, 2.24, 5.9];
        let ys: Vec<f64> = xs.iter().map(|x| -0.3 - 0.4 * x + 2.3 * x * x).collect();

        let polynom = least_squares(8, DVector::from(xs).column(0), DVector::from(ys).column(0));

        assert_float_eq!(polynom[0], -0.3, abs <= 0.00001);
        assert_float_eq!(polynom[1], -0.4, abs <= 0.00001);
        assert_float_eq!(polynom[2], 2.3, abs <= 0.00001);
        for a in polynom.iter().skip(3) {
            assert_float_eq!(*a, 0.0, abs <= 0.00001);
        }
    }

    #[test]
    fn identical_points() {
        let xs = vec![0.0, 2.0, 2.0];
        let ys = vec![0.0, 4.0, 4.0];

        let xs = DVector::from(xs);
        let ys = DVector::from(ys);

        let line = least_squares(2, xs.column(0), ys.column(0));
        let parabola = least_squares(3, xs.column(0), ys.column(0));

        dbg!(&line);
        dbg!(&parabola);
    }

    #[test]
    #[should_panic]
    fn different_lengths() {
        least_squares(3, dvector![3.0, 4.0].column(0), dvector![2.0].column(0));
    }

    #[test]
    fn vertical() {
        let xs = DVector::from(vec![0.0; 3]);
        let ys = dvector![7.0, 3.0, -1.0];

        let polynom = least_squares(1, xs.column(0), ys.column(0));
        assert_eq!(polynom.len(), 1);
        assert_float_eq!(polynom[0], 3.0, abs <= 0.00001);
    }
}
