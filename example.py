import random

def f(x):
    return 3 * x**2 + 5 * x - 10

for _ in range(1_000):
    x = random.uniform(-.1, .1)
    y = f(x)
    
    print(f"{x} {y}")
